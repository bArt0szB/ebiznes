import React, {Component} from 'react';
import {Tabs, Tab} from 'react-bootstrap'
import EditBasket from '../AdminForms/EditBasket/EditBasket'
import EditKeyword from '../AdminForms/EditKeyword/EditKeyword'
import EditOpinion from '../AdminForms/EditOpinion/EditOpinion'
import EditOrders from '../AdminForms/EditOrders/EditOrders'
import EditPays from '../AdminForms/EditPays/EditPays'
import EditProductForm from '../AdminForms/EditProductForm/EditProductForm'
import EditProductType from '../AdminForms/EditProductType/EditProductType'

class AdminPage extends Component{
    render(){
        return(
            <div className={'userPageBox'}>
                <h1 className={'header'}>
                    Strona admina
                </h1>
                <Tabs defaultActiveKey={6} id="uncontrolled-tab">
                    {/* <Tab eventKey={1} title="Edytuj koszyki">
                        <EditBasket/>
                    </Tab> */}
                    {/* <Tab eventKey={2} title="Edytuj slowa kluczowe">
                        <EditKeyword/>
                    </Tab> */}
                    <Tab eventKey={3} title="Opinie">
                        <EditOpinion/>
                    </Tab>
                    <Tab eventKey={4} title="Zamównienia">
                        <EditOrders/>
                    </Tab>
                    {/* <Tab eventKey={5} title="Płatności">
                        <EditPays/>
                    </Tab> */}
                    <Tab eventKey={6} title="Produkty">
                        <EditProductForm/>
                    </Tab>
                    <Tab eventKey={7} title="Kategorie">
                        <EditProductType/>
                    </Tab>
                </Tabs>
            </div>
        );
    }
}


export default AdminPage;
