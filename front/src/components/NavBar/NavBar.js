import React, {Component} from 'react';
import './NavBar.css'
import {Navbar, Nav, NavItem, PageHeader} from 'react-bootstrap'


class NavBar extends Component{
    render(){

        let loginout;
        let cart;
        let adm;

        if (sessionStorage.getItem('isAuth') && sessionStorage.getItem('isAuth') == 'true') {
            loginout = <NavItem eventKey={3} href='/' onClick={storageclear}>Wyloguj</NavItem>;
            cart = <NavItem eventKey={2} href='/userpage'>Koszyk</NavItem>;
            adm = <NavItem eventKey={4} href='/adminpage'>Admin</NavItem>;
          } else {
            loginout = <NavItem eventKey={3} href='http://localhost:9090/authenticate/google'>Zaloguj</NavItem>
            cart = null;
            adm = null;
          }

          function storageclear(e) {
            sessionStorage.clear();
          }

        return(
            <div>
                <Navbar>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <div className={'navLogo'}>
                                <a href={'/'}>
                                    <img src='https://www.maxelektro.pl/szablony/maxelektro/images/logo_glowne.svg' />
                                </a>
                            </div>
                        </Navbar.Brand>
                    </Navbar.Header>
                    <Nav>
                        <NavItem eventKey={1} href='/products'>
                            Produkty
                        </NavItem>
                        {cart}
                    </Nav>
                    <Nav pullRight>
                        {loginout}
                    </Nav>
                    <Nav pullRight>
                        {adm}
                    </Nav>
                </Navbar>
            </div>
        );
    }
}


export default NavBar;