import React, { Component } from 'react';
import { BasketData } from "../../Login/Login";
import { Grid, Row, Col } from 'react-bootstrap';


class Orders extends Component {
    constructor() {
        super();
        this.state = {
            compState: "Brak danych",
            pageTitle: "",
            fetchedOrders: [],
        };
        this.request_data();
    }


    request_data = async function () {
        var url = await "http://localhost:9090/orders/getorders/" + sessionStorage.getItem('basketID');

        const response = await fetch(url, {
            headers: { 'Access-Control-Allow-Origin': '*' },
            method: 'GET',
            mode: 'cors'
        });

        var json_response = await response.json();
        console.log(json_response);
        await this.setState({ fetchedOrders: json_response });

    };


    render() {
        if (!this.state.fetchedOrders.length) {
            return (
                <div>
                    <h1>{this.state.pageTitle}</h1>
                    <p>{this.state.compState}</p>
                </div>
            );
        } else {
            let orders = [];
            let counter = 0;
            this.state.fetchedOrders.forEach((el) => {
                counter++;
                orders.push(

                    <Col sm={2} md={2} lg={3}>
                        {/* <img className={'order'} src="https://cdn.onlinewebfonts.com/svg/img_290616.png" /> */}
                        <h3>Zamówienie nr. {counter}</h3>
                        <p>Id: {el.id}</p>
                        <h3 className={'price'}>Kwota: {el.total} zł</h3>
                    </Col>
                );
            });
            return (
                <div>
                    <h1>{this.state.pageTitle}</h1>
                    <Grid>{orders}</Grid>
                </div>
            );
        }
    }
}


export default Orders;
