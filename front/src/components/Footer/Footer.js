import React, {Component} from 'react';
import './Footer.css'


class Footer extends Component{
    render(){
        return(
            <div className={'bottomFooter'}>
                <footer>
                    <h1>
                    © 2018 Max Elektro
                    </h1>
                </footer>
            </div>
        );
    }
}

export default Footer;